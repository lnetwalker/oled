/*
 * OLED.h
 * basic commands to use 1.3" OLED 128x64 (128x32) white display
 * Version 1.0 supports OLED display's with either SDD1306 or SH1106 controller
 */

#ifndef oled_h
#define oled_h

#include <font.h>

#define OLED_address  0x3c                                // all the I2C OLED's I have seen have this address
#define SCREENBUFFERSIZE 8192                             // the screen buffer is 8192 Bits ( 128x64 )

class OledDisp {
  public:
    OledDisp(int offset, int OLEDwidth,int OLEDheight);
    // device specific functions
    void init_OLED(void);
    void displayOn(void);
    void displayOff(void);
    void reset_display(void);
    void clear_display(void);
    // text related functions
    void setXY(unsigned char row,unsigned char col);
    void setFont(int FontNr);
    void sendChar(unsigned char data);
    void sendCharXY(unsigned char data, int X, int Y);
    void sendStr(unsigned char *string);
    void sendStrXY( const char *string, int X, int Y);
    void sendBigCharXY(unsigned char data, int X, int Y);
    void sendBigStrXY( const char *string, int X, int Y);
    // graphics functions
    void deleteBuffer(void);
    void displayBuffer(void);
    void displayLine(int X1,int Y1,int X2,int Y2);
    void setPixel(int X,int Y);
    void drawCircle(int xc, int yc, int r);
    void circleBres(int xc, int yc, int x, int y);
    // special functions
    void heartbeat(int X,int Y);
    void sendcommand(unsigned char com);
  private:
    int _offset;
    int _Font;
    int _OLEDheight;
    int _OLEDwidth;
    boolean _buffer[SCREENBUFFERSIZE];
};

#endif
