#include "Arduino.h"
#include <oled.h>
#include <Wire.h>

#undef DEBUG_LED

  /// constructor of the display lib
  OledDisp::OledDisp(int offset, int OLEDwidth, int OLEDheight) // offset=0 for SSD1306 controller
  {                                                             // offset=2 for SH1106 controller
    // constructor
    _offset = offset;
    _Font = 1;
    _OLEDheight = OLEDheight;
    _OLEDwidth  = OLEDwidth;
  }

  //==========================================================//
  /// Inits oled and draws logo at startup
  void OledDisp::init_OLED(void)
  {
    sendcommand(0xae);    	  //display off
    sendcommand(0xa6);            //Set Normal Display (default)
    // Adafruit Init sequence for 128x64 OLED module
    sendcommand(0xAE);            //DISPLAYOFF
    sendcommand(0xD5);            //SETDISPLAYCLOCKDIV
    sendcommand(0x80);            // the suggested ratio 0x80
    sendcommand(0xA8);            //SSD1306_SETMULTIPLEX
    sendcommand(0x3F);
    sendcommand(0xD3);            //SETDISPLAYOFFSET
    sendcommand(0x0);             //no offset
    sendcommand(0x40 | 0x0);      //SETSTARTLINE
    sendcommand(0x8D);            //CHARGEPUMP
    sendcommand(0x14);
    sendcommand(0x20);            //MEMORYMODE
    sendcommand(0x00);            //0x0 act like ks0108

    //sendcommand(0xA0 | 0x1);    //SEGREMAP   //Rotate screen 180 deg
    sendcommand(0xA0);

    //sendcommand(0xC8);          //COMSCANDEC  Rotate screen 180 Deg
    sendcommand(0xC0);

    sendcommand(0xDA);            //0xDA
    if ( _OLEDheight == 32 ) {
      sendcommand(0x22);            //COMSCANDEC
      sendcommand(0x81);            //SETCONTRAS
      sendcommand(0x8F);            //
    } else {
      sendcommand(0x12);            //COMSCANDEC
      sendcommand(0x81);            //SETCONTRAS
      sendcommand(0xcF);            //
    }
    sendcommand(0xd9);            //SETPRECHARGE
    sendcommand(0xF1);
    sendcommand(0xDB);            //SETVCOMDETECT
    sendcommand(0x40);
    sendcommand(0xA4);            //DISPLAYALLON_RESUME
    sendcommand(0xA6);            //NORMALDISPLAY

    clear_display();
    sendcommand(0x2e);            // stop scroll
    //----------------------------REVERSE comments----------------------------//
    sendcommand(0xa0);            //seg re-map 0->127(default)
    sendcommand(0xa1);            //seg re-map 127->0
    sendcommand(0xc8);
    delay(1000);
    //----------------------------REVERSE comments----------------------------//
    //sendcommand(0xa7);         //Set Inverse Display
    // sendcommand(0xae);         //display off
    sendcommand(0x20);            //Set Memory Addressing Mode
    sendcommand(0x00);            //Set Memory Addressing Mode ab Horizontal addressing mode
    //  sendcommand(0x02);        // Set Memory Addressing Mode ab Page addressing mode(RESET)
    for ( int i=0; i<SCREENBUFFERSIZE; i++ ) {
      _buffer[i]=false;
    }

  }

  //==========================================================//
  /// Resets display depending on the actual mode.
  void OledDisp::reset_display(void)
  {
    displayOff();
    clear_display();
    displayOn();
  }

  //==========================================================//
  /// Turns display on.
  void OledDisp::displayOn(void)
  {
    sendcommand(0xaf);                          //display on
  }

  //==========================================================//
  /// Turns display off.
  void OledDisp::displayOff(void)
  {
    sendcommand(0xae);                          //display off
  }

  //==========================================================//
  /// Clears the display by sendind 0 to all the screen map.
  void OledDisp::clear_display(void)
  {
    unsigned char i,k;
    for(k=0;k<8;k++)
    {
      setXY(k,0);
      {
        for(i=0;i<(128 + 2 * _offset);i++)     //locate all COL
        {
          sendChar(0);                         //clear all COL
          //delay(10);
        }
      }
    }
  }

   //==========================================================//
  /// Set the cursor position in a 16 COL * 8 ROW map.
  void OledDisp::setXY(unsigned char row,unsigned char col)
  {
    sendcommand(0xb0+row);                    // set page address
    sendcommand(_offset+(8*col&0x0f));        // set low col address
    sendcommand(0x10+((8*col>>4)&0x0f));      // set high col address
  }

  //==========================================================//
  /// Actually this sends a byte, not a char to draw in the display.
  /// Display's chars uses 8 byte font the small ones and 32 bytes
  /// for the big number font.
  void OledDisp::sendChar(unsigned char data)
  {
    Wire.beginTransmission(OLED_address);     // begin transmitting
    Wire.write(0x40);                         // data mode
    Wire.write(data);
    Wire.endTransmission();                   // stop transmitting
  }

  //==========================================================//
  /// Prints a display char (not just a byte) in coordinates X Y,
  // being multiples of 8. This means we have 16 COLS (0-15)
  // and 8 ROWS (0-7).
  void OledDisp::sendCharXY(unsigned char data, int X, int Y)
  {
    setXY(X, Y);
    Wire.beginTransmission(OLED_address);     // begin transmitting
    Wire.write(0x40);                         // data mode

    for(int i=0;i<8;i++)
      if (_Font == 1 ) {
        Wire.write(pgm_read_byte(myFont[data-0x20]+i));
      } else {
        Wire.write(pgm_read_byte(altFont[data-0x20]+i));
      }

    Wire.endTransmission();                   // stop transmitting
  }

  //==========================================================//
  /// Prints a big display char (not just a byte) in coordinates X Y,
  // being multiples of 8. This means we have 8 COLS (0-7)
  // and 4 ROWS (0-3).
  void OledDisp::sendBigCharXY(unsigned char data, int X, int Y)
  {
    setXY(X, Y);
    Wire.beginTransmission(OLED_address);     // begin transmitting
    Wire.write(0x40);                         // data mode
    for(int i=0;i<16;i++)
        Wire.write(pgm_read_byte(bigFont[data-0x20]+i));
    Wire.endTransmission();                   // stop transmitting

    setXY(X+1, Y);
    Wire.beginTransmission(OLED_address);     // begin transmitting
    Wire.write(0x40);                         // data mode
    for(int i=16;i<32;i++)
        Wire.write(pgm_read_byte(bigFont[data-0x20]+i));

    Wire.endTransmission();                   // stop transmitting
  }

  //==========================================================//
  /// Prints a big string in coordinates X Y, being multiples of 8.
  // This means we have 8 COLS (0-7) and 4 ROWS (0-3).
  void OledDisp::sendBigStrXY( const char *string, int X, int Y)
  {
    unsigned char i=0;
    while(*string)
    {
      setXY(X,Y);
      for(i=0;i<16;i++)
          sendChar(pgm_read_byte(bigFont[*string-0x20]+i));
      setXY(X+1,Y);
      for(i=16;i<32;i++)
          sendChar(pgm_read_byte(bigFont[*string-0x20]+i));
      Y=Y+2;
      *string++;
    }
  }


  //==========================================================//
  /// Prints a string regardless the cursor position.
  void OledDisp::sendStr(unsigned char *string)
  {
    unsigned char i=0;
    while(*string)
    {
      for(i=0;i<8;i++)
      {
        if (_Font == 1 ) {
          sendChar(pgm_read_byte(myFont[*string-0x20]+i));
        } else {
          sendChar(pgm_read_byte(altFont[*string-0x20]+i));
        }
      }
      *string++;
    }
  }

  //==========================================================//
  /// Prints a string in coordinates X Y, being multiples of 8.
  // This means we have 16 COLS (0-15) and 8 ROWS (0-7).
  void OledDisp::sendStrXY( const char *string, int X, int Y)
  {
    setXY(X,Y);
    unsigned char i=0;
    while(*string)
    {
      for(i=0;i<8;i++)
      {
        if (_Font == 1 ) {
          sendChar(pgm_read_byte(myFont[*string-0x20]+i));
        } else {
          sendChar(pgm_read_byte(altFont[*string-0x20]+i));
        }
      }
      *string++;
    }
  }

  /// select a font by number
  void OledDisp::setFont(int FontNr)
  {
    _Font = FontNr;
  }

  /// clear the internal display buffer
  void OledDisp::deleteBuffer(void) {
    for ( int i=0; i<SCREENBUFFERSIZE; i++ ) {
      _buffer[i]=false;
    }
  }

  /// write the contents of the display buffer to the display
  void OledDisp::displayBuffer(void) {
    for ( int textrow=0; textrow<8;textrow++) { 		        // loop over 8 textcolumns with 8 graphic lines
      setXY(textrow,0);
      for ( int col=0;col<128; col++ ) { 			              // loop over 128 cols in a ROW
        byte pixelbyte=0;
	      for ( int pixelbit=0; pixelbit<8;pixelbit++) {		  // loop over the 8 graph lines of aa textrow
          if (_buffer[textrow*1024+pixelbit*128+col]) {
            pixelbyte=pixelbyte+pow(2,pixelbit);
          }
        }
        sendChar(pixelbyte);
      }
    }
  }

  /// draw a line on the display
  void OledDisp::displayLine(int x1,int y1,int x2,int y2) {

    int x,y,dx,dy,dx1,dy1,px,py,xe,ye,i;
    dx=x2-x1;
    dy=y2-y1;
    dx1=fabs(dx);
    dy1=fabs(dy);
    px=2*dy1-dx1;
    py=2*dx1-dy1;
    if(dy1<=dx1){
      if(dx>=0) {
        x=x1;
        y=y1;
        xe=x2;
      } else {
        x=x2;
        y=y2;
        xe=x1;
      }
      setPixel(x,y);
      for(i=0;x<xe;i++){
        x=x+1;
          if(px<0){
            px=px+2*dy1;
          } else {
            if((dx<0 && dy<0) || (dx>0 && dy>0)){
              y=y+1;
            } else {
              y=y-1;
            }
            px=px+2*(dy1-dx1);
          }
          setPixel(x,y);
        }
      } else {
        if(dy>=0){
          x=x1;
          y=y1;
          ye=y2;
        } else {
          x=x2;
          y=y2;
          ye=y1;
        }
        setPixel(x,y);
        for(i=0;y<ye;i++) {
          y=y+1;
          if(py<=0) {
            py=py+2*dx1;
          } else {
            if((dx<0 && dy<0) || (dx>0 && dy>0)){
              x=x+1;
            } else {
              x=x-1;
            }
            py=py+2*(dx1-dy1);
          }
          setPixel(x,y);
        }
      }
    }

  /// Function to put pixels at subsequence points
  void OledDisp::circleBres(int xc, int yc, int x, int y)
  {
      setPixel(xc+x, yc+y);
      setPixel(xc-x, yc+y);
      setPixel(xc+x, yc-y);
      setPixel(xc-x, yc-y);
      setPixel(xc+y, yc+x);
      setPixel(xc-y, yc+x);
      setPixel(xc+y, yc-x);
      setPixel(xc-y, yc-x);
  }

  /// draw a circle on the display
  // Function for circle-generation
  // using Bresenham's algorithm
  void OledDisp::drawCircle(int xc, int yc, int r)
  {
      int x = 0, y = r;
      int d = 3 - 2 * r;
      while (y >= x)
      {
          // for each pixel we will
          // draw all eight pixels
          circleBres(xc, yc, x, y);
          x++;

          // check for decision parameter
          // and correspondingly
          // update d, x, y
          if (d > 0)
          {
              y--;
              d = d + 4 * (x - y) + 10;
          }
          else
              d = d + 4 * x + 6;
          circleBres(xc, yc, x, y);
          delay(50);
      }
  }

  /// print a pixel on display
  void OledDisp::setPixel(int X,int Y) {
    if ( X < _OLEDwidth && Y < _OLEDheight ) {
      _buffer[X+Y*128] = true;
    }
  }

  /// print a heartbeat on display
  void OledDisp::heartbeat(int X,int Y) {
    static int beatcnt=0;
    beatcnt++;
    if (beatcnt == 25) {
  #ifdef DEBUG_LED
      digitalWrite(BUILTIN_LED, LOW);
      delay(50);
      digitalWrite(BUILTIN_LED, HIGH);
      delay(25);
      digitalWrite(BUILTIN_LED, LOW);
      delay(50);
      digitalWrite(BUILTIN_LED, HIGH);
      delay(25);
  #endif
      setFont(2);                                              // use the alternate font to display the heart
      sendStrXY("!",X,Y);
      delay(50);
      sendStrXY("\"",X,Y);
      delay(50);
      sendStrXY("!",X,Y);
      delay(25);
      sendStrXY("\"",X,Y);
      delay(50);
      sendStrXY("!",X,Y);
      delay(50);
      sendStrXY(" ",X,Y);
      setFont(1);
      beatcnt=0;
    }
  }

 //==========================================================//
  /// send commands to the display
  void OledDisp::sendcommand(unsigned char com)
  {
    Wire.beginTransmission(OLED_address);     // begin transmitting
    // ToDo check command mode for 0.91" OLED 128x32 pixel. Is it 0x0 ?
    // see Adafruit_SSD1306.cpp line 321 ff
    if ( _OLEDheight == 32) {
      Wire.write(0x00);                         // command mode
    } else {
      Wire.write(0x80);                         // command mode
    }
    Wire.write(com);
    Wire.endTransmission();                   // stop transmitting
  }


