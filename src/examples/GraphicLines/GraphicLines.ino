

#include <Arduino.h>
#include <Wire.h>
#define DisplayType 1                                               // 0 = no Display; 1 = small 1,3" OLED; 2 = Digole 320*240 Display
#include <oled.h>                                                   // driver for OLED Display                                                 

// Display
OledDisp myOLED(0);                                                 // define new Display 
                                                                    // offset=2 for SH1106 controller
                                                                    // offset=0 for SSD1306 controller
// define physical screen width
const int MaxX = 15;
const int MaxY = 7;
#define I2C_SCL 5                                                   // I2C Setup: use GPIO 4 and 5 for SDA and SCL
#define I2C_SDA 4                                                   // these are D1 and D2 of the nodemcu board

void setup() {
  Serial.begin(115000);                                             // open the Serial port
  Wire.begin(I2C_SDA, I2C_SCL);                                     // Initialize I2C 
  myOLED.init_OLED();                                               // init display                            
  myOLED.reset_display();
  myOLED.clear_display();
  myOLED.displayOn();
}

void loop() {
  Serial.println("nxt");
  myOLED.clear_display();
  myOLED.sendBigStrXY("12:34",2,4);
  delay(1000);
  myOLED.deleteBuffer();
  myOLED.displayLine(10,53,117,10);
  myOLED.displayLine(10,10,117,53);
  myOLED.displayLine(0,31,127,31);
  myOLED.displayLine(0,20,0,42);
  myOLED.displayLine(127,20,127,42);  
  myOLED.setPixel(0,0);
  myOLED.setPixel(127,0);
  myOLED.setPixel(127,63);
  myOLED.setPixel(0,63);
  myOLED.drawCircle(64,31,15);
  myOLED.drawCircle(64,31,20);
  myOLED.drawCircle(64,31,25);
  myOLED.drawCircle(64,31,30);
  myOLED.drawCircle(64,31,35);
  myOLED.displayBuffer();
  delay(2000);
}
