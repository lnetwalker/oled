/*
 * Testprogram for OLED library
 * 
 */

#include <Arduino.h>
#include "main.h"
#include "oled.h"
#include <Wire.h>

#define I2C_SCL 5                                                   // I2C Setup: use GPIO 4 and 5 for SDA and SCL
#define I2C_SDA 4                                                   // these are D1 and D2 of the nodemcu board

#define SDD1306 0
#define SH1106  2

#define SCREENHEIGHT 64
// Display
OledDisp myOLED(SH1106, 128, SCREENHEIGHT);                              // define new Display 

int i;

void setup() {
  Serial.begin(115200);                                             // open the Serial port
  Serial.println("OLED test starting...");
  Serial.print("init I2C...");
  Wire.begin(I2C_SDA, I2C_SCL);                                     // Initialize I2C 
  Serial.println("done");

  Serial.print("init OLED...");
  myOLED.init_OLED();                                               // init display                            
  Serial.println("done");
  Serial.print("reset OLED...");
  myOLED.reset_display();
  Serial.println("done");
  Serial.print("switch on OLED...");
  myOLED.displayOn();
  Serial.println("done");
  Serial.print("clear OLED...");
  myOLED.clear_display();
  Serial.println("done");
  delay(1000);
  i=0;
}

void loop() {
  i++;
  myOLED.sendStrXY("oled test..",2,0); 
  delay(1000);
  String Zeit;
  Zeit = "23:"+String(i);
//  myOLED.sendBigCharXY(Zeit.c_str(),0,0);
  delay(1000);
  myOLED.deleteBuffer();
  myOLED.displayLine(0,0,64,0);
  myOLED.displayLine(64,0,64,SCREENHEIGHT-1);
  myOLED.displayLine(64,SCREENHEIGHT-1,0,SCREENHEIGHT-1);
  myOLED.displayLine(0,SCREENHEIGHT-1,0,0);
  myOLED.displayLine(10,SCREENHEIGHT-1,10,0);
  myOLED.displayLine(0,0,10,0);
  myOLED.displayBuffer();
  delay(1000);
  myOLED.deleteBuffer();
  myOLED.drawCircle(64, int(SCREENHEIGHT/2), int(SCREENHEIGHT/2)-2);
  myOLED.displayBuffer();
  delay(1000);
  myOLED.deleteBuffer();
  myOLED.setPixel(64, int(SCREENHEIGHT/2));
  myOLED.displayBuffer();
  delay(1000);
  Serial.print(".");
}