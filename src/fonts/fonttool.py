#!/usr/bin/python

# fonttool.py
#
# (c) 2019 by Hartmut Eilers <hartmut@eilers.net>
# released under the conditions of the GNU GPL V 2.0
#
# this tool is designed to help handling fonts for the
# small 1106 and 1306 based OLED displays
# can be used to generate the hex fontdata for 8*8 and 16*16 pixels
# characters from charmaps ( see empty.char and empty.char8 )
# and vice versa you can create character maps from the hexcodes of a char

import sys
import os
from array import *

class ReadFontFile:

   def __init__(self,path):
      self.path = path

   def add_data2array(self,fontsize):
      # build the array in respect of the font size
      char_array = [[0 for x in range(0,fontsize)] for y in range(0,fontsize)]
      with open(self.path, 'r') as f:
         row_cnt = 0
         for line in f:
            col_cnt = 0
            for c in line:
               if ( col_cnt < fontsize ):
                  sys.stdout.write(c)
                  char_array[col_cnt][row_cnt] = c
               else:
                  print
               col_cnt = col_cnt + 1
            row_cnt = row_cnt +1

         self.char_array = char_array

   def calculate_font_data(self,fontsize):
      # if it is a 8 byte font we just need to iterate once for all 8 bytes
      # if its a 32 byte font we need to iterate over the first 16 bytes
      for col in range(fontsize):
         # we need to calculate the vertical oriented bytes for every column in the array
         # the low nibble are the upper four bits, the high nibble are the lower four bits
         # see readme for more information
         data = int(self.char_array[col][0]) * 1 + int(self.char_array[col][1]) * 2 + int(self.char_array[col][2]) * 4 + int(self.char_array[col][3]) * 8 \
         + int(self.char_array[col][4]) * 16 + int(self.char_array[col][5]) * 32 + int(self.char_array[col][6]) * 64 + int(self.char_array[col][7]) * 128
         sys.stdout.write(hex(data))
         sys.stdout.write(",")
      # its a 32 byte font so we need a second iteration over the remaining 16 bytes
      if (fontsize>8):
         for col in range(16):
            data = int(self.char_array[col][8]) * 1 + int(self.char_array[col][9]) * 2 + int(self.char_array[col][10]) * 4 + int(self.char_array[col][11]) * 8 \
            + int(self.char_array[col][12]) * 16 + int(self.char_array[col][13]) * 32 + int(self.char_array[col][14]) * 64 + int(self.char_array[col][15]) * 128
            sys.stdout.write(hex(data))
            sys.stdout.write(",")

   def hextobin(self,hexval):
      # Takes a string representation of hex data with
      # arbitrary length and converts to string representation
      # of binary.  Includes padding 0s
      thelen = len(hexval)*2        # 2 means 8 bits representation, 4 means 16 bits
      binval = bin(int(hexval, 16))[2:]
      while ((len(binval)) < thelen):
         binval = '0' + binval
      return binval

   def generateCharmap(self,bytearray):
      # we have hex fontdata and want to print the ascii char map
       data_amount = len(bytearray)           # check wether it's 8 or 16 bit
       if (data_amount > 8 ):
           data_amount = 16
           #print data_amount
           my_array = [[0 for x in range(0,data_amount)] for y in range(0,data_amount)]   # generate an empty array
           # fill array with pixel data
           col = 0
           for entry in bytearray:
              bytevalue = reader.hextobin(entry)[::-1]   # [::-1] reverses the string !
              #print bytevalue
              for row in range(0,8):
                 if (col<8):
                    my_array[row][col] = bytevalue[row]
                 elif (col < 16):
                    my_array[row][col] = bytevalue[row]
                 elif (col < 24):
                    my_array[row + 8][col-16] = bytevalue[row]
                 else:
                    my_array[row + 8][col-16] = bytevalue[row]
              col = col + 1
           #print

           # now spit out the charactermap
           for row in range(0,data_amount):
               #print row
               for col in range(0,data_amount):
                  sys.stdout.write(my_array[row][col])
               print



if __name__ == '__main__':

   # there is a argument, treat it as filename
   if len(sys.argv) > 1:
      filepath = sys.argv[1]
   else:
      # spit out error, we need a file to process
      print("char file as parameter needed")
      sys.exit()

   if not os.path.isfile(filepath):
      # there is something wrong with the given file
      print("File path {} does not exist. Exiting...".format(filepath))
      sys.exit()

   # generate a reader
   reader = ReadFontFile(filepath)

   # check which kind of file do we have (charactermap 8, charactermap 16, hexdata 8 or hexdata 16)
   with open(filepath) as f:
      first_line = f.readline().strip()

      if ( first_line[0:2] == "0x" ):   # it's hexdata
         bytearray = first_line.split(",")      # read all data
         reader.generateCharmap(bytearray)

      else:
         # its a char map, calculate the hex values
         # check the size of font ( 8 or 16 )
         fontsize=len(first_line)

         # read the file and build up array
         reader.add_data2array(fontsize)
         print
         # calculate the font data
         reader.calculate_font_data(fontsize)
         print
