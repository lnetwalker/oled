upper half of char
1................     \                  \
2................     | low nibble       |
4................     |                  |
8................     /                  |
                                          >     first 16 bytes for the complete row
1................     \                  |
2................     | high nibble      |
4................     |                  |
8................     /                  /

lower half of char
1................     \                  \
2................     | low nibble       |
4................     |                  |
8................     /                  |
                                          >     second 16 bytes for the complete row
1................     \                  |
2................     | high nibble      |
4................     |                  |
8................     /                  /


Large Font:

* line length is 16 dots 
* a 0 means no pixel a 1 means a pixel
* for large font the bytes are calculated for the 
  complete row, every byte contains the upper 4 
  rows in low nibble, the lower 4 rows in high nibble
* the first 16 bytes ( 0-15) are the upper half of char,
  the second 16 bytes ( 16-31) are the lower half of char.