FontTool

helper programm for small ssd1306 and 1106 controler based displays,
to generate fonts.
Can be used to generate the hexcodes of a character from a character map
or generate the charmap from hexcodes
Can work with 8x8 and 16x16 font files

usage:

fonttool.py <charmap>
fonttool.py <hexmap>

for the charmap files there are examples, see empty.char and empty.char8

Examples:

fonttool.py big_9.bytemap

generates from the 32 bytes in big_9.bytemap a character map
